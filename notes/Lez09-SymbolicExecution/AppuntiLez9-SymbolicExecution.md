# Symbolic execution

Come sappiamo, il software è estremamente prono a bugs, e per trovarli è necessario un estensivo testing e code review. Tuttavia questo non basta, infatti alcuni bugs non vengono trovati perchè 

- in feature raramente usate
- in circostanze particolari
- a causa del non-determinismo

A questo proposito ci viene incontro la **static analysis**, la quale permette di analizzare tutti le possibili esecuzioni di un programma. Questa tecnica di analisi ha dato origine a innumerevoli software e tool di aiuto allo sviluppo allo scopo di migliorare la qualità del software: ma quindi è la panacea universale?

No, purtroppo. Come in tutte le cose, non esiste un silver bullet (MongaDB docet) e bisogna bilanciare la sensitività (trovando bug difficili) e la probabilità di trovare falsi positivi.

L'astrazione ci permette di modellare tutte le possibili esecuzioni, ma con essa si introduce il conservatismo (?) ...

## Symbolic execution : a middle ground

Seguiamo ora una strada alternativa: sfruttiamo le asserzioni.

*Remember* : ***Proprietà analizzatori***

- **Sound** : implica che l'analisi non produce falsi negativi, ovvero 
  - se l'analisi dice che c'è un bug, non è detto che sia vero perchè potrei avere dei falsi positivi
  - se l'analisi dice che non ci sono bug allora è sicuramente vero
- **Complete** : implica che l'analisi non produce falsi positivi, ovvero 
  - se l'analisi dice che c'è un bug allora è sicuramente vero
  - se l'analisi dice che non ci sono bugs, non è detto che sia vero perchè potrei avere dei falsi negativi

Non esistono analizzatori sia sound che complete (perfetti), ma cercano di bilanciare fra le due qualità.

Usando le asserzioni sono sicuro che i bug che trovo sono bug reali (quindi sono più complete che sound) poichè ogni test explora solo una possibile soluzione 

```c
assert(f(3) == 5)
```

Generalmente speriamo che i test case siano il più generalizzabili possibili: a questo proposito la symbolic execution ci viene incontro assegnando dei simboli a ogni variabile responsabile di un cambiamento di flusso e simulando tutti i path dell'albero delle esecuzioni possibili (constraint solver)

*Esempio*:

```c
int a = α, b = β, c = γ;
// symbolic
int x = 0, y = 0, z = 0;
if (a) 
   x = -2;

if (b < 5) 
{
    if (!a && c) 
       y = 1; 

    z = 2;
}
assert(x+y+z != 3)
```

![SymExecTree](SymExecTree.png)

*Il costraint solver, insieme ai path di esecuzione, assomigliano molto al Prolog* (duh prolog è un constraint solver)

Ogni symbolic execution path è comune a molte esecuzioni di un programma (questo perchè c'è esattamente un set di esecuzione che soddisfa quella path condition. La symbolic execution quindi copre l'execution space del programma MOLTO più del testing tradizionale. 

La symbolic execution è, rispetto alla static analysis:

- Complete, ma non sound (usualmente non termina per i troppi path)
- Path, flow and context sensitive

L'idea della symbolic execution esiste da molto tempo (~1975) ma il fatto che sia molto compute-intensive l'ha lasciata nel dimenticatoio fino ad ora. Fortunatamente ora i pc sono più potenti, a prezzi affordable e con migliori algoritmi di costrain solving (SMT/SAT). Si riescono quindi a risolvere istanze velocemente, checkando le asserzioni e tagliando (prune) i path infeasible.

...

---

## Search and SMT

La symbolic execution è semplice e utile ma è computazionalmente costosa. Vedremo come l'uso effettivo dell'esecuzione
